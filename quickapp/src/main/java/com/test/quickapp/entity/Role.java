package com.test.quickapp.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.cache.annotation.Cacheable;

@Entity
@Table(name = "role",
        uniqueConstraints = {@UniqueConstraint(columnNames = "role_name")})
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Role implements Serializable{
	
	@Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(generator = "role_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "role_seq", sequenceName = "role_id_seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @Column(name = "role_name", unique = true)
    private String roleName;
    
    @Column(name = "enabled")
    private Boolean enabled = true;
    

	public Role() {}
	
	public Role(String roleName, Boolean enabled) {
        this.roleName = roleName;
        this.enabled = enabled;
    }
	
	@Override
    public String toString() {
        return "Role [id=" + id + ", roleName=" + roleName + ", enabled=" + "]";
    }
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return Objects.equals(id, role.id) &&
                Objects.equals(roleName, role.roleName);
    }
	
	@Override
    public int hashCode() {
        return Objects.hash(id, roleName);
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	


}
