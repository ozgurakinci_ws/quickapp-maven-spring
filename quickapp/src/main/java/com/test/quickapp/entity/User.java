package com.test.quickapp.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


@Entity
@Table(name = "user", uniqueConstraints = {@UniqueConstraint(columnNames = "username"), @UniqueConstraint(columnNames = "email_address")})
public class User implements Serializable, UserDetails{
	
	@Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(generator = "user_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "user_seq", sequenceName = "user_seq_id_seq",
            initialValue = 1, allocationSize = 1)
	private Long id;
	
	
	@Column(name = "username", unique = true, nullable = false)
    private String username;
	
	@Column(name = "email_address", unique = true, nullable = false)
    private String emailAddress;
	
	@Column(name = "password", nullable = false)
    private String password;
	
	@Column(name = "enabled")
    private Boolean enabled = false;
	
	@Column(name = "active")
    private Boolean active = false;
	
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, targetEntity = UserRole.class,
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
            orphanRemoval = true)
    private Set<UserRole> userRoles = new HashSet<>();
	
	
	
    public User() {
    }

    public User(String username, String emailAddress, String password, Boolean enabled, Boolean active) {
        this.username = username;
        this.emailAddress = emailAddress;
        this.password = password;
        this.enabled = enabled;
        this.active = active;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(username, user.username);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(id, username);
    }
    
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", password='" + password + '\'' +
                ", enabled=" + enabled +
                 ", active=" + active +
                '}';
    }
    
    public void addRole(Role rol, Long loggedInUserId) {
    	UserRole userRole = new UserRole(this, rol);
        this.userRoles.add(userRole);
    }
    
    public void removeRole(Role role) {
        for (Iterator<UserRole> iterator = userRoles.iterator(); iterator.hasNext(); ) {
            UserRole userRole = iterator.next();
            if (userRole.getUser().equals(this) && userRole.getRole().equals(role)) {
                iterator.remove();
                userRole.setUser(null);
                userRole.setRole(null);
            }
        }
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Set<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}
    
	@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (UserRole ur : this.getUserRoles()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(ur.getRole().getRoleName()));
        }
        return grantedAuthorities;
    }

	@Override
	public boolean isAccountNonExpired() {
		return this.enabled;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return this.enabled;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.active;
	}

    

}
