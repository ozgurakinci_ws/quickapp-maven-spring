package com.test.quickapp.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "user_role")
public class UserRole implements Serializable{
	@EmbeddedId
    private UserRolComposite id;
	
	
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @MapsId("userId")
    private User user;
	
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Role.class)
    @MapsId("roleId")
    private Role role;
	
	
	public UserRole() {
		
	}
	
	public UserRole(User user, Role role) {
        this.user = user;
        this.role = role;
        this.id = new UserRolComposite(user.getId(), role.getId());
    }
	
	
	
	
	public UserRolComposite getId() {
		return id;
	}

	public void setId(UserRolComposite id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}




	@Embeddable
    public static class UserRolComposite implements Serializable {

        @Column(name = "user_id")
        private Long userId;

        @Column(name = "role_id")
        private Long roleId;

        public UserRolComposite() {
        }

        public UserRolComposite(Long userId, Long roleId) {
            this.userId = userId;
            this.roleId = roleId;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getRoleId() {
            return roleId;
        }

        public void setRoleId(Long roleId) {
            this.roleId = roleId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            UserRolComposite that = (UserRolComposite) o;
            return Objects.equals(userId, that.userId) &&
                    Objects.equals(roleId, that.roleId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(userId, roleId);
        }

        @Override
        public String toString() {
            return "UserRolComposite{" +
                    "userId=" + userId +
                    ", roleId=" + roleId +
                    '}';
        }
    }

}
