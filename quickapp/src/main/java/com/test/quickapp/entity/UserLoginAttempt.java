package com.test.quickapp.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "user_login_attempt")
public class UserLoginAttempt implements Serializable {

    @Id
    @Column(name = "id", updatable = false, unique = true)
    @GeneratedValue(generator = "login_attempt_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "login_attempt_seq", sequenceName = "user_login_attempt_id_seq",
            initialValue = 1, allocationSize = 1)
    private Long id;

    @Column(name = "username", nullable = false, updatable = false)
    private String username;

    @Column(name = "success", nullable = false, updatable = false)
    private boolean success = false;

    @Column(name = "ip_address", nullable = false, updatable = false)
    private String ipaddress;

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = "try_date", updatable = false)
    private Date trydate;

    public UserLoginAttempt() {
    }

    public UserLoginAttempt(String username, boolean success, String ipaddress) {
        this.username = username;
        this.success = success;
        this.ipaddress = ipaddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserLoginAttempt that = (UserLoginAttempt) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "UserLoginAttempt{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", success=" + success +
                ", ip_address='" + ipaddress + '\'' +
                ", try_date=" + trydate +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public Date getTrydate() {
		return trydate;
	}

	public void setTrydate(Date trydate) {
		this.trydate = trydate;
	}

    
}