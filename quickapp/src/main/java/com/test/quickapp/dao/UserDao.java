package com.test.quickapp.dao;

import com.test.quickapp.entity.User;

public interface UserDao extends BaseDao<User, Long>{

	public User findUserByUsername(String username);
	

}
