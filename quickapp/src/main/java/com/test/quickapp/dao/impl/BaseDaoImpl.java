package com.test.quickapp.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.test.quickapp.dao.BaseDao;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings({"SpringJavaAutowiredFieldsWarningInspection", "SpringJavaInjectionPointsAutowiringInspection", "WeakerAccess"})
@Repository
public abstract class BaseDaoImpl<T extends Serializable, PK extends Serializable> implements BaseDao<T, PK> {

    private final Class<T> entityClass;

    @Autowired
    private SessionFactory sessionFactory;

    protected BaseDaoImpl(final Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    protected Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public T persist(final T entity) {
        sessionFactory.getCurrentSession().persist(entityClass.getSimpleName(), entity);
        return entity;
    }

    @Override
    public T update(final T entity) {
        sessionFactory.getCurrentSession().update(entityClass.getSimpleName(), entity);
        return entity;
    }

    @Override
    public T merge(final T entity) {
        sessionFactory.getCurrentSession().merge(entityClass.getSimpleName(), entity);
        return entity;
    }

    @Override
    public void delete(final T entity) {
        sessionFactory.getCurrentSession().delete(entityClass.getSimpleName(), entity);
    }

    @Override
    public void deleteById(final PK id) {
        Query q = sessionFactory.getCurrentSession()
                .createQuery("DELETE FROM " + entityClass.getSimpleName() + " t WHERE t.id=:prmId");
        q.setParameter("prmId", id);
        q.executeUpdate();
    }

    @Override
    public void deleteAllById(final List<PK> ids) {
        StringBuilder sb = new StringBuilder("DELETE FROM ").append(entityClass.getSimpleName());
        sb.append(" t WHERE t.id").append(ids.size() > 1 ? " IN " : "=").append("(:prm)");
        Query q = sessionFactory.getCurrentSession().createQuery(sb.toString());
        q.setParameter("prm", ids);
        q.executeUpdate();
    }

    @Override
    public void findAndDeleteById(final PK id) {
        delete(findById(id));
    }

    @Override
    public void findAndDeleteAllById(final List<PK> ids) {
        for (PK id : ids) {
            findAndDeleteById(id);
        }
    }

    @Override
    public T findById(final PK id) {
        return sessionFactory.getCurrentSession().find(entityClass, id);
    }

    @Override
    public List<T> findAll() {
        return sessionFactory.getCurrentSession().createQuery("FROM " + entityClass.getSimpleName(), entityClass)
                .getResultList();
    }

    @Override
    public List<T> findAllById(final List<PK> ids) {
        StringBuilder sb = new StringBuilder("FROM ").append(entityClass.getSimpleName());
        sb.append(" t WHERE t.id").append(ids.size() > 1 ? " IN " : "=").append("(:prm)");

        Query<T> q = sessionFactory.getCurrentSession().createQuery(sb.toString(), entityClass);
        q.setParameter("prm", ids);

        return q.getResultList();
    }

    @Override
    public List<T> findAllByPage(final Integer pageSize, final Integer page) {
        Query<T> q = sessionFactory.getCurrentSession().createQuery("FROM " + entityClass.getSimpleName(), entityClass);
        q.setFetchSize((page - 1) * pageSize);
        q.setMaxResults(pageSize);
        return q.getResultList();
    }
}