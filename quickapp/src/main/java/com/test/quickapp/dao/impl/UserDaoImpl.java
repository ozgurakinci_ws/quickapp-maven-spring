package com.test.quickapp.dao.impl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.test.quickapp.dao.UserDao;
import com.test.quickapp.entity.User;

@SuppressWarnings("JpaQlInspection")
@Repository
public class UserDaoImpl extends BaseDaoImpl<User, Long> implements UserDao{

	protected UserDaoImpl() {
		super(User.class);
	}

	@Override
	public User findUserByUsername(final String username) {
		String hql = "SELECT t FROM user t WHERE t.username=:prm";

        Query<User> k = getCurrentSession().createQuery(hql, User.class);
        k.setParameter("prm", username);
        return QueryResultHelper.getSingleResultOrNull(k);
	}

}
