package com.test.quickapp.dao;

import java.io.Serializable;
import java.util.List;

public interface BaseDao<T extends Serializable, PK extends Serializable> {
    T findById(final PK id);

    T persist(final T entity);

    T update(final T entity);

    T merge(T entity);

    void delete(final T entity);

    void deleteById(final PK id);

    void findAndDeleteById(final PK id);

    void findAndDeleteAllById(final List<PK> ids);

    void deleteAllById(final List<PK> ids);

    List<T> findAll();

    List<T> findAllById(final List<PK> ids);

    List<T> findAllByPage(final Integer pageSize, final Integer page);

}