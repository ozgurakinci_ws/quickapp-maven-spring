package com.test.quickapp.dao;

import com.test.quickapp.entity.UserLoginAttempt;

public interface UserLoginAttemptDao extends BaseDao<UserLoginAttempt, Long> {

}
