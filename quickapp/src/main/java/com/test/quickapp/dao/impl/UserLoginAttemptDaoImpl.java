package com.test.quickapp.dao.impl;

import org.springframework.stereotype.Repository;

import com.test.quickapp.dao.UserLoginAttemptDao;
import com.test.quickapp.entity.UserLoginAttempt;

@Repository
public class UserLoginAttemptDaoImpl extends BaseDaoImpl<UserLoginAttempt, Long> implements UserLoginAttemptDao {

    protected UserLoginAttemptDaoImpl() {
        super(UserLoginAttempt.class);
    }
}
