package com.test.quickapp.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.core.annotation.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.authentication.www.DigestAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.DigestAuthenticationFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig{
	
	// Base Configurations
	private PasswordEncoder getNoOpPasswordEncoder() {
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                return rawPassword.toString();
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return rawPassword.toString().equals(encodedPassword);
            }
        };
    }
	
	 @Bean
     public PasswordEncoder getPasswordEncoder() {
        Map<String, PasswordEncoder> encoders = new HashMap<>();
        encoders.put("bcrypt", new BCryptPasswordEncoder(12));
        encoders.put("noop", this.getNoOpPasswordEncoder());

        DelegatingPasswordEncoder encoder = new DelegatingPasswordEncoder("bcrypt", encoders);
        encoder.setDefaultPasswordEncoderForMatches(encoders.get("noop"));
        return encoder;
     }
	 
	 
	@Bean
    public CharacterEncodingFilter getCharacterEncodingFilter() {
        CharacterEncodingFilter f = new CharacterEncodingFilter();
        f.setEncoding("UTF-8");
        f.setForceEncoding(true);
        f.setForceResponseEncoding(true);
        f.setForceRequestEncoding(true);
        return f;
    }
	
	
	
	// MVC Web Security Config
	public static class MVCWebSecurityConfig extends WebSecurityConfigurerAdapter {
		private final CharacterEncodingFilter characterEncodingFilter;
        private final UserDetailsService      userDetailsService;
        private final PasswordEncoder         passwordEncoder;
        private final MDCFilter               mdcFilter;
	       
        @Autowired
        public MVCWebSecurityConfig(CharacterEncodingFilter characterEncodingFilter,
                                    UserDetailsService userDetailsService,
                                    PasswordEncoder passwordEncoder,
                                    MDCFilter mdcFilter) {
            this.characterEncodingFilter = characterEncodingFilter;
            this.userDetailsService = userDetailsService;
            this.passwordEncoder = passwordEncoder;
            this.mdcFilter = mdcFilter;
        }
        
        
        @Override
        protected void configure(HttpSecurity http) throws Exception {
        	http.addFilterAfter(mdcFilter, FilterSecurityInterceptor.class);
            http.addFilterBefore(characterEncodingFilter, CsrfFilter.class);
            http.authorizeRequests()
            		
                    .antMatchers("/user/**").hasAnyRole("USER", "ADMIN")
                    .antMatchers("/", "/rest/**").permitAll()
                    .and().formLogin().loginPage("/login/").loginProcessingUrl("/authenticationUser")
                    .failureUrl("/login?error=e").defaultSuccessUrl("/").permitAll()
                    .and().logout().logoutSuccessUrl("/").permitAll()
                    .and().authorizeRequests().anyRequest().authenticated();
        }
		
		@Override
	    public void configure(WebSecurity ws) {
	        ws.ignoring().antMatchers("/resources/**");
	    }
		
		@Override
        protected void configure(AuthenticationManagerBuilder auth) {
            auth.authenticationProvider(this.getDaoAuthenticationProvider());
        }
		
		@Bean
        public DaoAuthenticationProvider getDaoAuthenticationProvider() {
            DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
            authProvider.setUserDetailsService(this.userDetailsService);
            authProvider.setPasswordEncoder(this.passwordEncoder);
            return authProvider;
        }
		
		

	}
	
	
	// Rest Web Security Config
	@SuppressWarnings("ConstantConditions")
    @Order(1)
    public static class RESTWebSecurityConfig extends WebSecurityConfigurerAdapter {

        private final UserDetails             userDetails;
        private final CharacterEncodingFilter characterEncodingFilter;
        private final MDCFilter               mdcFilter;

        @Autowired
        public RESTWebSecurityConfig(CharacterEncodingFilter characterEncodingFilter,
                                     MDCFilter mdcFilter) {
            this.characterEncodingFilter = characterEncodingFilter;
            this.userDetails = User.withUsername("rest-username")
                    .password("rest-user-password")
                    .roles("rest-user-roles").build();
            this.mdcFilter = mdcFilter;
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.addFilterBefore(characterEncodingFilter, CsrfFilter.class);
            http.addFilterAfter(getDigestAuthenticationFilter(), BasicAuthenticationFilter.class);
            http.addFilterAfter(mdcFilter, FilterSecurityInterceptor.class);

            http.csrf().ignoringAntMatchers("/rest/api/**");

            http.exceptionHandling().authenticationEntryPoint(getDigestAuthenticationEntryPoint())
                    .and()
                    .antMatcher("/rest/api/**")
                    .authorizeRequests()
                    .anyRequest().hasRole("REST_USER");
            http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.inMemoryAuthentication().withUser(userDetails);
        }

        @Bean
        public DigestAuthenticationFilter getDigestAuthenticationFilter() {
            DigestAuthenticationFilter filter = new DigestAuthenticationFilter();
            filter.setUserDetailsService(new InMemoryUserDetailsManager(userDetails));
            filter.setAuthenticationEntryPoint(getDigestAuthenticationEntryPoint());
            filter.setPasswordAlreadyEncoded(false);
            return filter;
        }

        @Bean
        public DigestAuthenticationEntryPoint getDigestAuthenticationEntryPoint() {
            DigestAuthenticationEntryPoint entryPoint = new DigestAuthenticationEntryPoint();
            entryPoint.setRealmName("rest-realm-name");
            entryPoint.setKey("rest-realm-password");
            return entryPoint;
        }
    }
	

	
}
