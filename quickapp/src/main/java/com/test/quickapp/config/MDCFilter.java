package com.test.quickapp.config;

import org.slf4j.MDC;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class MDCFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String username;
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                username = authentication.getName();
            } else {
                username = "Anonymous";
            }

            String ipAddress = "Unknown";
            String sessionId = "null";
            if (authentication.getDetails() instanceof WebAuthenticationDetails) {
                ipAddress = ((WebAuthenticationDetails) authentication.getDetails()).getRemoteAddress();
                sessionId = ((WebAuthenticationDetails) authentication.getDetails()).getSessionId();
            }

            String mdcData = String.format("[user:%s ### ip:%s ### sid:%s] ", username, ipAddress, sessionId);
            MDC.put("userData", mdcData);
            filterChain.doFilter(request, response);
        } finally {
            MDC.remove("userData");
        }
    }
}