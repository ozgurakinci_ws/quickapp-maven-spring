package com.test.quickapp.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;

import com.test.quickapp.entity.User;

public class Utils {

	public Utils() {}

	public static String getClientIP(HttpServletRequest request) {
		String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
	}
	
	public static User getCurrentlyActiveUser() {
        try {
            return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (ClassCastException ex) {
            return null;
        }
    }
	
	 public static Long getCurrentlyActiveUserId() {
	        try {
	            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	            if (principal instanceof User) {
	                User user = (User) principal;
	                if (user.getUsername().equals("orbis")) {
	                    return 0L;
	                } else {
	                    return null;
	                }
	            } else if (principal instanceof User) {
	                return ((User) principal).getId();
	            } else {
	                return -99L;
	            }
	        } catch (Exception ex) {
	            return null;
	        }
	    }

}
