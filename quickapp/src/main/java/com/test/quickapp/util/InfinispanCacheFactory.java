package com.test.quickapp.util;

import org.infinispan.Cache;
import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

public class InfinispanCacheFactory<K extends Serializable, V extends Serializable> {

    private final String cacheName;

    public InfinispanCacheFactory(String cacheName) {
        this.cacheName = cacheName;
    }

    public Cache<K, V> buildCache() {
        DefaultCacheManager manager = getDefaultCacheManager();
        manager.defineConfiguration(this.cacheName, getCacheConfiguration());
        return manager.getCache(this.cacheName);
    }

    private DefaultCacheManager getDefaultCacheManager() {
        return new DefaultCacheManager();
    }

    private Configuration getCacheConfiguration() {
        return new ConfigurationBuilder()
                .expiration()
                .lifespan(1, TimeUnit.HOURS)
                .build();
    }
}
