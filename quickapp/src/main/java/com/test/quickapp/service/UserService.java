package com.test.quickapp.service;

import com.test.quickapp.entity.User;

public interface UserService {
	
	User findUserByUsername(String username);
	
	
	
}
