package com.test.quickapp.service.impl;

import org.infinispan.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.quickapp.dao.UserDao;
import com.test.quickapp.entity.User;
import com.test.quickapp.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	private final UserDao     userDao;

	@Autowired
    public UserServiceImpl(UserDao userDao) {
		this.userDao = userDao;
	}
	
	@Override
    public User findUserByUsername(final String username) {
        return userDao.findUserByUsername(username);
    }

	
	
}