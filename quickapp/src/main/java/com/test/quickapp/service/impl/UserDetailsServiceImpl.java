package com.test.quickapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.quickapp.dao.UserDao;
import com.test.quickapp.service.UserLoginAttemptService;
import com.test.quickapp.util.Utils;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
    private final UserDao            userDao;
    private final HttpServletRequest      request;
    private final UserLoginAttemptService userLoginAttemptService;

    @Autowired
    public UserDetailsServiceImpl(UserDao userDao,
                                  HttpServletRequest request,
                                  UserLoginAttemptService userLoginAttemptService) {
        this.userDao = userDao;
        this.request = request;
        this.userLoginAttemptService = userLoginAttemptService;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        if (userLoginAttemptService.isBlocked(Utils.getClientIP(request))) {
            throw new RuntimeException("You have entered a lot of attempts and have been temporarily blocked. Try again later.");
        } else {
            return userDao.findUserByUsername(s);
        }
    }
}