package com.test.quickapp.service;

public interface UserLoginAttemptService {
	void loginEvent(boolean isSuccess, String username, String ipAddress);
    boolean isBlocked(String ipAdress);
    
}