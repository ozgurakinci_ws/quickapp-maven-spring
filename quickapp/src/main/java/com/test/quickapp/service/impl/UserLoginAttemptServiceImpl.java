package com.test.quickapp.service.impl;

import org.infinispan.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.quickapp.dao.UserLoginAttemptDao;
import com.test.quickapp.entity.UserLoginAttempt;
import com.test.quickapp.service.UserLoginAttemptService;
import com.test.quickapp.util.InfinispanCacheFactory;

@Service
public class UserLoginAttemptServiceImpl implements UserLoginAttemptService {
	private final UserLoginAttemptDao userLoginAttemptDao;
	private final Cache<String, Short>    cache;
    private final Environment             env;
	
    @Autowired
	public UserLoginAttemptServiceImpl(UserLoginAttemptDao userLoginAttemptDao, Environment env) {
		this.userLoginAttemptDao = userLoginAttemptDao;
		this.env = env;
		cache = new InfinispanCacheFactory<String, Short>("loginAttemp").buildCache();
	}

	@Override
	@Transactional
	public void loginEvent(boolean isSuccess, String username, String ipAddress) {
		UserLoginAttempt g = new UserLoginAttempt();
        g.setUsername(username);
        g.setIpaddress(ipAddress);
        g.setSuccess(isSuccess);

        if (isSuccess) {
            cache.remove(ipAddress);
        } else {
            short attempts;
            if (cache.get(ipAddress) != null) {
                attempts = cache.get(ipAddress);
            } else {
                attempts = 0;
            }
            attempts++;
            cache.put(ipAddress, attempts);
        }

        if (!env.getProperty("excluded.users").contains(username)) {
        	userLoginAttemptDao.persist(g);
        }
	}

	
	public boolean isBlocked(String ipAdress) {
		if (cache.get(ipAdress) != null) {
            return cache.get(ipAdress) >= 10;
        } else {
            return false;
        }
	}

}
