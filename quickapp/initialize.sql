-- PostgreSQL Initialize 

CREATE USER quickapp WITH
	LOGIN
	SUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'quickapp';
GRANT postgres TO quickapp;

CREATE DATABASE "quickapp"
    WITH 
    OWNER = quickapp
    ENCODING = 'UTF8'
    LC_COLLATE = 'Turkish_Turkey.1254'
    LC_CTYPE = 'Turkish_Turkey.1254'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
GRANT CREATE, CONNECT ON DATABASE "quickapp" TO quickapp;
GRANT TEMPORARY ON DATABASE "quickapp" TO quickapp WITH GRANT OPTION;
GRANT TEMPORARY, CONNECT ON DATABASE "quickapp" TO PUBLIC;